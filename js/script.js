const namecheck = document.getElementById('name'); // je crée une const qui va cibler l'element html via l'id
const formulaire = document.getElementById('signup-form'); // je crée une const qui va cibler l'element html via l'id
const emailcheck = document.getElementById('email'); // je crée une const qui va cibler l'element html via l'id
const phonecheck = document.getElementById('phone'); // je crée une const qui va cibler l'element html via l'id
const errorname = document.createElement('span'); // je crée une const qui va qui va créée un span dans l'html
errorname.appendChild(document.createTextNode('Please enter your name')) // je met un .appendChild a l'element créée par la const le .createTextNode me permet de choisir le message d'erreur a affiché. donc la on a un span qui est parent du message
const errormail = document.createElement('span');
errormail.appendChild(document.createTextNode('Please enter your email'))
const errorphone = document.createElement('span');
errorphone.appendChild(document.createTextNode('Please enter your phone number, it must start with 06 or 07'))

function name_verif() {
    const check_name_error = namecheck.nextElementSibling;
    if (namecheck.value == "" || namecheck.value.length > 50) { // Si (if) l'element ciblé par ma const a une valeur egal a rien
        if (check_name_error == null) { //il existe pas donc on l'affiche
            namecheck.parentElement.appendChild(errorname); // alors je cible le parent de l'element ciblé par ma const, grace au .parentElement et je dis a ce parent de créé un enfant via la const "errormsg" donc ça crée le span > message d'erreur
            namecheck.style.border = "2px solid red"
        }
    } else {
        namecheck.style.border = "2px solid lightgreen";

        console.log(check_name_error);
        if (check_name_error !== null) {
            check_name_error.remove();
        }
    }
}

function email_verif() {
    const check_email_error = emailcheck.nextElementSibling;
    if (emailcheck.value == "") {
        if (check_email_error == null) {
            emailcheck.parentElement.appendChild(errormail);
            emailcheck.style.border = "2px solid red"
        }
    } else {
        emailcheck.style.border = "2px solid lightgreen"
        if (check_email_error !== null) {
            check_email_error.remove();
        }
    }
}

function phone_verif() {
    const check_phone_error = phonecheck.nextElementSibling;
    const phone_format = /^(0)[67][0-9]{8}$/gm;
    if (phonecheck.value == "" || !phonecheck.value.match(phone_format)) {
        if (check_phone_error == null) {
            phonecheck.parentElement.appendChild(errorphone);
            phonecheck.style.border = "2px solid red"
        }
    } else {
        phonecheck.style.border = "2px solid lightgreen"
        if (check_phone_error !== null) {
            check_phone_error.remove();
        }
    }
}

function validateForm() {
    name_verif();
    email_verif();
    phone_verif();
}

const submitbtn = document.getElementById('submit'); // je crée une const qui va cibler l'element html via l'id
submitbtn.addEventListener('click', (event) => { // je met un addEventListener de type click a ma const, donc au moment du click sur l'element ciblé par la const un event se déclenche
    event.preventDefault(); // je met un preventDefault a l'event se qui évite a la page de se refresh
    validateForm(); // et j'execute la fonction validateForm
});

namecheck.addEventListener('keyup', () => {
    name_verif()
})

phonecheck.addEventListener('keyup', () => {
    phone_verif()
})